import { PluginObject } from "vue";

import { default as aclApi, AclApi } from "./acl/mods";

export interface Api {
  aclApi: AclApi;
}

export const ApiPlugin: PluginObject<Api> = {
  install: (Vue) => {
    Object.defineProperties(Vue.prototype, {
      $api: {
        get() {
          return {
            aclApi,
          };
        },
      },
    });
  },
};
