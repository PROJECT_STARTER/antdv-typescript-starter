/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc actions
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  id: number,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: Array<acl.Action>;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/module/${id}/actions`,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
