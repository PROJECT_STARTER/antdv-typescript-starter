/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc delete
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export default async function (
  id: number,

  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: void;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "delete",
    url: `/action/${id}`,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
