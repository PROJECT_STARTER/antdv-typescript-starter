/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc captcha
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export class CaptchaParams {
  constructor(public length?: number) {}
}

export default async function (
  params: CaptchaParams,
  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: string;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/captcha`,

    params,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
