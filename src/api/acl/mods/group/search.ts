/* eslint-disable @typescript-eslint/no-explicit-any */
/**
 * @desc search
 */
import { defaultSuccess, defaultError, http } from "@/plugins/axios";

export class SearchParams {
  constructor(
    public key?: string,
    public page?: number,
    public size?: number,
    public state?: boolean
  ) {}
}

export default async function (
  params: SearchParams,
  success: ({
    data,
    ext,
    state,
    errors,
  }: {
    data: acl.Pagination<acl.Group>;
    ext: ObjectMap;
    state: "SUCCESS" | "FAIL" | "EXCEPTION";
    errors?: Array<string>;
  }) => void = defaultSuccess,
  fail: (error: string) => void = defaultError
): Promise<void> {
  return await http({
    method: "get",
    url: `/groups`,

    params,
  })
    .then((data) => success(data as any))
    .catch((error) => fail(error));
}
